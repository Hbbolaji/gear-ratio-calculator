import React, { useState } from "react";

const FWD_RWD = () => {
  const [data, setData] = useState({
    Rw: 1 * 1,
    Fr: 1 * 1,
    Te: 1 * 1,
    Mv: 1 * 1,
    a: 50 * 1,
    b: 50 * 1,
    hl: 1 * 1,
    μp: 1 * 1,
    ηd: 1 * 1,
  });
  const [result, setResult] = useState(null);
  const [toggle, setToggle] = useState("front");
  const [K, setK] = useState(null);
  const onChangeRadio = (e) => {
    setToggle(e.target.value);
  };

  const onChange = (e) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
  };

  const calculate = () => {
    let K;
    let num;
    let den;
    if (toggle === "front") {
      num = data.a / 100 + data.Fr * data.hl;
      den = 1 + data.μp * data.hl;
      K = num / den;
      setK(K);
    } else if (toggle === "rear") {
      num = data.b / 100 - data.Fr * data.hl;
      den = 1 - data.μp * data.hl;
      K = num / den;
      setK(K);
    }
    const mid = data.Rw / (data.ηd * data.Te);
    const res = K * mid * data.μp * data.Mv * 9.81;
    setResult(res);
  };
  return (
    <>
      {result && (
        <div style={{ width: "50%", padding: "2px", margin: "auto" }}>
          <h4>
            {toggle} K: {K.toFixed(2)}
          </h4>
          <h4>Gear Ratio: {result.toFixed(2)}</h4>
        </div>
      )}
      <form>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Tyre Rolling Radius (Rw) Meters</label>
            <input
              className="input"
              type="number"
              value={data.Rw}
              name="Rw"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">
              Coefficient of Rolling Resistance (fr)
            </label>
            <input
              className="input"
              type="number"
              value={data.Fr}
              name="Fr"
              onChange={onChange}
            />
          </div>
        </div>
        <div>
          <label className="label">Engine Maximum Torque (Te) N.M</label>
          <input
            className="input"
            type="number"
            value={data.Te}
            name="Te"
            onChange={onChange}
          />
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Vehicle Mass (Mv) Kg</label>
            <input
              className="input"
              type="number"
              value={data.Mv}
              name="Mv"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Vehicle Weight (Wv) N</label>
            <input
              className="input"
              type="number"
              value={(data.Mv * 9.81).toFixed(2)}
              disabled
            />
          </div>
        </div>

        <h3>Weight Distribution</h3>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Front (a)</label>
            <span style={{ fontSize: "12px" }}>
              Distance from centroid of the car to the center of the FW
            </span>
            <input
              className="input"
              type="number"
              value={data.a}
              name="a"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Rear (b)</label>
            <span style={{ fontSize: "12px" }}>
              Distance from centroid of the car to the center of the RW
            </span>
            <input
              className="input"
              type="number"
              value={data.b}
              name="b"
              onChange={onChange}
            />
          </div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">CG height to wheelbase ratio (h/l)</label>
            <input
              className="input"
              type="number"
              value={data.hl}
              name="hl"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Max Raod Adhesion Coefficient (μp)</label>
            <input
              className="input"
              type="number"
              value={data.μp}
              name="μp"
              onChange={onChange}
            />
          </div>
        </div>
        <div>
          <label className="label">Driveline Efficiency (ηd)</label>
          <input
            className="input"
            type="number"
            value={data.ηd}
            name="ηd"
            onChange={onChange}
          />
        </div>
        <div style={{ paddingTop: "20px", paddingBottom: "20px" }}>
          <input
            type="radio"
            checked={toggle === "front"}
            name="frontrear"
            value="front"
            onChange={onChangeRadio}
          />{" "}
          Front
          <input
            type="radio"
            checked={toggle === "rear"}
            name="frontrear"
            value="rear"
            onChange={onChangeRadio}
          />{" "}
          Rear
        </div>
        <div>
          <button
            onClick={(e) => {
              e.preventDefault();
              calculate();
            }}
          >
            Calculate
          </button>

          <button
            onClick={(e) => {
              setK(null);
              setResult(null);
              setData({
                Rw: 1 * 1,
                Fr: 1 * 1,
                Te: 1 * 1,
                Mv: 1 * 1,
                a: 50 * 1,
                b: 50 * 1,
                hl: 1 * 1,
                μp: 1 * 1,
                ηd: 1 * 1,
              });
            }}
          >
            Clear
          </button>
        </div>
      </form>
    </>
  );
};

export default FWD_RWD;
