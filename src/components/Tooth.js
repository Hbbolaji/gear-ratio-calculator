import React, { useState } from "react";

const Tooth = () => {
  const [result, setResult] = useState(null);
  const [data, setData] = useState({
    nL: 1,
    nH: 1,
    i: 1,
    N: 2,
  });
  const onChange = (e) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
  };
  const calculate = () => {
    const num = (data.N - 1) * data.nL * data.nH;
    const b = (data.N - data.i) * data.nH;
    const den = b + (data.i - 1) * data.nL;
    setResult(num / den);
  };
  return (
    <div>
      {result && (
        <div style={{ width: "50%", padding: "40px", margin: "auto" }}>
          <h1>Intermediate Gear Ratio: {result.toFixed(2)}</h1>
        </div>
      )}
      <form>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Lowest Gear (nL)</label>
            <input
              className="input"
              type="number"
              value={data.nL}
              name="nL"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Highest Gear (nH)</label>
            <input
              className="input"
              type="number"
              value={data.nH}
              name="nH"
              onChange={onChange}
            />
          </div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Gear Speed (i)</label>
            <input
              className="input"
              type="number"
              value={data.i}
              name="i"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Number of Gears (N)</label>
            <input
              className="input"
              type="number"
              value={data.N}
              name="N"
              onChange={onChange}
            />
          </div>
        </div>
        <div>
          <button
            className="button"
            onClick={(e) => {
              e.preventDefault();
              calculate();
            }}
          >
            Calculate
          </button>
        </div>
      </form>
    </div>
  );
};

export default Tooth;
