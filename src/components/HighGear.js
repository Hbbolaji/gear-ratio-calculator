import React, { useState } from "react";

const HighGear = () => {
  const [force, setForce] = useState(null);
  const [power, setPower] = useState(null);
  const [result, setResult] = useState(null);
  const [data, setData] = useState({
    C: 1 * 1,
    Fr: 1 * 1,
    Mv: 1 * 1,
    Vmax: 1 * 1,
    ηd: 1 * 1,
    We: 1 * 1,
    Rw: 1 * 1,
  });
  const onChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const calculate = () => {
    const v = (data.Vmax * 1000) / 3600;
    const F = data.Fr * data.Mv * 9.81 + data.C * v * v;
    setForce(F);
    const P = F * data.ηd * v;
    setPower(P);
    const a = data.Rw / v;
    const res = data.We * a * (Math.PI / 30);
    setResult(res);
  };
  return (
    <>
      {result && (
        <div style={{ width: "50%", padding: "2px", margin: "auto" }}>
          <h4>Force: {force.toFixed(2)}</h4>
          <h4>Power: {power.toFixed(2)}</h4>
          <h4>Gear Ratio: {result.toFixed(2)}</h4>
        </div>
      )}
      <form>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Aerodynamic Coefficient (C)</label>
            <input
              className="input"
              type="number"
              value={data.C}
              name="C"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">
              Coefficient of Rolling Resistance (fr)
            </label>
            <input
              className="input"
              type="number"
              value={data.Fr}
              name="Fr"
              onChange={onChange}
            />
          </div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Tyre Rolling Radius (Rw) Meters</label>
            <input
              className="input"
              type="number"
              value={data.Rw}
              name="Rw"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Driveline Efficiency (ηd)</label>
            <input
              className="input"
              type="number"
              value={data.ηd}
              name="ηd"
              onChange={onChange}
            />
          </div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">Vehicle Mass (Mv) Kg</label>
            <input
              className="input"
              type="number"
              value={data.Mv}
              name="Mv"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Vehicle Weight (Wv) N</label>
            <input
              className="input"
              type="number"
              value={(data.Mv * 9.81).toFixed(2)}
              disabled
            />
          </div>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>
            <label className="label">
              Maximum Velocity of Vehicle (Vmax) m/s
            </label>
            <input
              className="input"
              type="number"
              value={data.Vmax}
              name="Vmax"
              onChange={onChange}
            />
          </div>
          <div>
            <label className="label">Engine Speed (We) Rpm</label>
            <input
              className="input"
              type="number"
              value={data.We}
              name="We"
              onChange={onChange}
            />
          </div>
        </div>
        <div>
          <button
            onClick={(e) => {
              e.preventDefault();
              calculate();
            }}
          >
            Calculate
          </button>

          <button
            onClick={(e) => {
              setResult(null);
              setPower(null);
              setForce(null);
              setData({
                C: 1 * 1,
                Fr: 1 * 1,
                Mv: 1 * 1,
                Vmax: 1 * 1,
                ηd: 1 * 1,
                We: 1 * 1,
                Rw: 1 * 1,
              });
            }}
          >
            Clear
          </button>
        </div>
      </form>
    </>
  );
};

export default HighGear;
