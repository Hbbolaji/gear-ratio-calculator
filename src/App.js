import { useState } from "react";
import "./App.css";
import FWD_RWD from "./components/FWD_RWD";
import Tooth from "./components/Tooth";
import HighGear from "./components/HighGear";

function App() {
  const [method, setMethod] = useState("fr");
  return (
    <div className="container">
      <h1 style={{ marginBottom: "50px", color: "white" }}>
        Gear Ratio Calculator by Zaid Almatari (1624115) Desktop Only
      </h1>
      <div style={{ marginBottom: "50px" }}>
        <button onClick={() => setMethod("fr")}>Low Gear Ratio Method</button>
        <button onClick={() => setMethod("tooth")}>Gear tooth Method</button>
        <button onClick={() => setMethod("high")}>
          High Gear Ratio Method
        </button>
      </div>
      <div className="formContainer">
        {method === "fr" && <FWD_RWD />}
        {method === "tooth" && <Tooth />}
        {method === "high" && <HighGear />}
      </div>
    </div>
  );
}

export default App;
